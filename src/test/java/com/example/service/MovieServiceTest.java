package com.example.service;

import com.example.dto.OmdbData;
import com.example.model.Movie;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

@QuarkusTest
class MovieServiceTest {

    @Inject
    MovieService movieService;

    static List<Movie> movies = new ArrayList<>();
    static OmdbData omdbData = new OmdbData();

    static Logger logger = LoggerFactory.getLogger(MovieServiceTest.class);


    @BeforeEach
    void beforeEach() {
        PanacheMock.mock(Movie.class);
        movies = List.of(
                new Movie(UUID.randomUUID().toString(), "Game of Thrones", "2011–2019", "Movies", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie(UUID.randomUUID().toString(), "The Imitation Game", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie(UUID.randomUUID().toString(), "Sherlock Holmes: A Game of Shadows", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947")
        );

        omdbData.setTitle("Game of Thrones");
        omdbData.setYear("2011–2019");
        omdbData.setType("Movies");
        omdbData.setPoster("https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg");
        omdbData.setImdbID("tt0944947");
    }

    @Test
    void testGetAll() {
        PanacheMock.doReturn(movies).when(Movie.class).listAll();
        assertEquals(movies, movieService.getAll());
        PanacheMock.verify(Movie.class, times(1)).listAll();
    }

    @Test
    @Transactional
    void testSave() throws BadRequestException{
        //success
        Movie movie = movies.get(0);
        PanacheMock.doNothing().when(Movie.class).persist(movie);
//        PanacheMock.doReturn(Boolean.TRUE).when(Movie.class).isPersistent();

        Movie result = movieService.save(movie);
        assertEquals(movie.imdbID, result.imdbID);

        //failure
//        PanacheMock.doReturn(false).when(Movie.class).isPersistent();
//        assertThrows(BadRequestException.class, () -> movieService.save(movie));
    }

}