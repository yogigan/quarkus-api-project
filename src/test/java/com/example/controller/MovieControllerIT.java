package com.example.controller;


import com.example.model.Movie;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

@QuarkusTest
@TestHTTPEndpoint(MovieController.class)
public class MovieControllerIT {

    @BeforeEach
    @Transactional
    void setUp() {
        Movie.deleteAll();
        Movie.persist(List.of(
                new Movie("Game of Thrones", "2011–2019", "Movies", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie("The Imitation Game", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie("Sherlock Holmes: A Game of Shadows", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947")
        ));
    }

    @Test
    void testGetAll() {
        Response response = when().get("/list")
                .then()
                .statusCode(200)
                .body("size()", equalTo(3))
                .body("title", hasItems("Game of Thrones", "The Imitation Game", "Sherlock Holmes: A Game of Shadows"))
                .body("year", hasItems("2011–2019", "2011–2019", "2011–2019")).extract().response();
    }

    @Test
    void testGetBySearchQuery() {
        String query = "Thrones";
        when().get("/search?q=" + query)
                .then()
                .statusCode(200)
                .body("Search.Title", hasItem("Game of Thrones"));


    }


    @Test
    void testGetDetailByOmdbIdAndTitleAndYear() {
        String omDbId = "tt0944947";
        String year = "2011–2019";
        String title = "Game of Thrones";

        when().get("?omdbId=" + omDbId + "&title=" + title + "&year=" + year)
                .then()
                .statusCode(200)
                .body("Title", equalTo("Game of Thrones"))
                .body("Year", equalTo("2011–2019"))
                .body("imdbID", equalTo("tt0944947"));
    }

    @Test
    void testSaveMovie() {
        //success
        Movie movie = new Movie();
        movie.title = "Game of Thrones";
        movie.year = "2011–2019";
        movie.poster = "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg";
        movie.imdbID = "tt0944947";
        movie.type = "Movies";

        given()
                .contentType(ContentType.JSON)
                .body(movie)
                .when().post("/save")
                .then()
                .statusCode(201);
    }

}
