package com.example.controller;

import com.example.dto.OmdbData;
import com.example.dto.OmdbResponse;
import com.example.model.Movie;
import com.example.proxy.MovieProxy;
import com.example.service.MovieService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;

@QuarkusTest
@TestHTTPEndpoint(MovieController.class)
class MovieControllerTest {

    @InjectMock
    MovieService movieService;

    @InjectMock(convertScopes = true)
    MovieProxy movieProxy;

    static List<Movie> movies = new ArrayList<>();
    static OmdbData omdbData = new OmdbData();
    static OmdbResponse omdbResponse = new OmdbResponse();

    @BeforeEach
    void beforeEach() {
        movies = List.of(
                new Movie(UUID.randomUUID().toString(), "Game of Thrones", "2011–2019", "Movies", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie(UUID.randomUUID().toString(), "The Imitation Game", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947"),
                new Movie(UUID.randomUUID().toString(), "Sherlock Holmes: A Game of Shadows", "2011–2019", "Series", "https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg", "tt0944947")
        );

        omdbData.setTitle("Game of Thrones");
        omdbData.setYear("2011–2019");
        omdbData.setPoster("https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg");
        omdbData.setImdbID("tt0944947");
        omdbData.setType("Movies");


        omdbResponse.setTotalResults(1);
        omdbResponse.setResponse("True");
        omdbResponse.setSearch(List.of(omdbData));

    }

    @Test
    void testGetAll() {
        Mockito.when(movieService.getAll()).thenReturn(movies);

        when().get("/list")
                .then()
                .statusCode(200)
                .body("size()", equalTo(3))
                .body("title", hasItems("Game of Thrones", "The Imitation Game", "Sherlock Holmes: A Game of Shadows"))
                .body("year", hasItems("2011–2019", "2011–2019", "2011–2019"));

        Mockito.verify(movieService, times(1)).getAll();
    }

    @Test
    void testGetBySearchQuery() {
        String query = "Thrones";
        Mockito.when(movieProxy.getBySearch(query)).thenReturn(omdbResponse);
        when().get("/search?q=" + query)
                .then()
                .statusCode(200)
                .body("Search.size()", equalTo(1))
                .body("Search.Title", hasItem("Game of Thrones"))
                .body("Search.Year", hasItem("2011–2019"));
        Mockito.verify(movieProxy, times(1)).getBySearch(query);
    }

    @Test
    void testGetDetailByOmdbIdAndTitleAndYear() {
        String omDbId = "tt0944947";
        String year = "2011–2019";
        String title = "Game of Thrones";

        Mockito.when(movieProxy.getByOmdbIdAndTitleAndYear(omDbId, title, year)).thenReturn(omdbData);

        when().get("?omdbId=" + omDbId + "&title=" + title + "&year=" + year)
                .then()
                .statusCode(200)
                .body("Title", equalTo("Game of Thrones"))
                .body("Year", equalTo("2011–2019"))
                .body("imdbID", equalTo("tt0944947"));

        Mockito.verify(movieProxy, times(1)).getByOmdbIdAndTitleAndYear(omDbId, title, year);
    }

    @Test
    void testSaveMovie() {
        //success
        Movie movie = movies.get(0);
        movie.id = null;
        Mockito.when(movieService.save(movie)).thenReturn(movies.get(0));
        given()
                .contentType(ContentType.JSON)
                .body(movie)
                .when().post("/save")
                .then()
                .statusCode(201);
        Mockito.verify(movieService, times(1)).save(any());

        //failed bad request
        doThrow(new BadRequestException("Movie is not valid")).when(movieService).save(any());
        given()
                .contentType(ContentType.JSON)
                .body(movie)
                .when().post("/save")
                .then()
                .statusCode(400);
        Mockito.verify(movieService, times(2)).save(any());
        assertThrows(BadRequestException.class, () -> movieService.save(movie));

    }

}