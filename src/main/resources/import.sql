INSERT INTO tb_movie(id, title, year, type, poster, imdbId)
VALUES ('3812c3ec-df0f-11ec-9d64-0242ac120002', 'Game of Thrones', '2011–2019', 'Action, Adventure, Drama',
'https://m.media-amazon.com/images/M/MV5BYTRiNDQwYzAtMzVlZS00NTI5LWJjYjUtMzkwNTUzMWMxZTllXkEyXkFqcGdeQXVyNDIzMzcwNjc@._V1_SX300.jpg',
'tt0944947'),
('44fc5618-df0f-11ec-9d64-0242ac120002', 'The Imitation Game', '2014', 'Biography, Drama, Thriller',
'https://m.media-amazon.com/images/M/MV5BOTgwMzFiMWYtZDhlNS00ODNkLWJiODAtZDVhNzgyNzJhYjQ4L2ltYWdlXkEyXkFqcGdeQXVyNzEzOTYxNTQ@._V1_SX300.jpg',
'tt2084970'),
('4a5eaaca-df0f-11ec-9d64-0242ac120002', 'Sherlock Holmes: A Game of Shadows', '2011', 'Action, Adventure, Mystery',
'https://m.media-amazon.com/images/M/MV5BMTQwMzQ5Njk1MF5BMl5BanBnXkFtZTcwNjIxNzIxNw@@._V1_SX300.jpg', 'tt1515091');