package com.example.dto;


public class BasicResponse {

    String message;
    String code;
    String status;

    public BasicResponse() {
    }

    public BasicResponse(String message, String code, String status) {
        this.message = message;
        this.code = code;
        this.status = status;
    }

    public static BasicResponse ok(String message) {
        return new BasicResponse(message, "200", "OK");
    }

    public static BasicResponse error(String message) {
        return new BasicResponse(message, "500", "Internal Server Error");
    }

    public static BasicResponse badRequest(String message) {
        return new BasicResponse(message, "400", "Bad Request");
    }

    public static BasicResponse created(String message) {
        return new BasicResponse(message, "201", "Created");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BasicResponse{" +
                "message='" + message + '\'' +
                ", code='" + code + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
