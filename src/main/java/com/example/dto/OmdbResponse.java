package com.example.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class OmdbResponse {

    @JsonProperty("Search")
    private List<OmdbData> search;

    private int totalResults;

    @JsonProperty("Response")
    private String response;

    public List<OmdbData> getSearch() {
        return search;
    }

    public void setSearch(List<OmdbData> search) {
        this.search = search;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}