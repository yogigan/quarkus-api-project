package com.example.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tb_movie")
public class Movie extends PanacheEntityBase {

    @Id
    @SequenceGenerator(name = "movie_generator", sequenceName = "movie_sequence_id", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    public String id;

    @Column(length = 200, nullable = false)
    public String title;

    @Column(length = 11)
    public String year;

    @Column(length = 200)
    public String type;

    @Column(length = 200)
    public String poster;

    @Column(length = 200)
    public String imdbID;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Movie() {
    }

    public Movie(String title, String year, String type, String poster, String imdbID) {
        this.title = title;
        this.year = year;
        this.type = type;
        this.poster = poster;
        this.imdbID = imdbID;
    }

    public Movie(String id, String title, String year, String type, String poster, String imdbID) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.type = type;
        this.poster = poster;
        this.imdbID = imdbID;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", type='" + type + '\'' +
                ", poster='" + poster + '\'' +
                ", imdbID='" + imdbID + '\'' +
                '}';
    }
}
