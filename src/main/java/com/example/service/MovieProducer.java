package com.example.service;

import com.example.model.Movie;
import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MovieProducer {

    static final Logger LOGGER = LoggerFactory.getLogger(MovieProducer.class);
    @Inject
    @Channel("movies-out")
    Emitter<Record<String, Movie>> emitter;

    public void sendMovieToKafka(Movie movie) {
        LOGGER.info("Send a movie {} - {}", movie.year, movie.title);
        emitter.send(Record.of(movie.imdbID, movie));
    }
}
