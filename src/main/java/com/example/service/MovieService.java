package com.example.service;

import com.example.model.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.BadRequestException;
import java.util.List;

@ApplicationScoped
public class MovieService {

    static Logger log = LoggerFactory.getLogger(MovieService.class);

    public List<Movie> getAll() {
        return Movie.listAll();
    }

    @Transactional
    public Movie save(Movie movie) {
        log.info("Saving movie: {}", movie);

        if ((movie.title == null || movie.title.isEmpty()) ||
                (movie.year == null || movie.year.isEmpty()) ||
                (movie.poster == null || movie.poster.isEmpty()) ||
                (movie.type == null || movie.type.isEmpty()) ||
                (movie.imdbID == null || movie.imdbID.isEmpty())) {
            throw new BadRequestException("Movie not valid");
        }

        movie.id = null;
        Movie.persist(movie);
        return movie;
    }


}
