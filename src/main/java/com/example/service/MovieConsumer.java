package com.example.service;

import com.example.model.Movie;
import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.UUID;

@ApplicationScoped
public class MovieConsumer {

    @Inject
    MovieService movieService;

    private final Logger logger = LoggerFactory.getLogger(MovieConsumer.class);

    @Transactional
    @Incoming("movies-in")
    public void receive(Record<String, Movie> record) {
        logger.info("Got a movie: {} - {}", record.key(), record.value());
        movieService.save(record.value());
    }
}
