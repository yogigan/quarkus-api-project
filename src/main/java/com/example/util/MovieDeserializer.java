package com.example.util;

import com.example.model.Movie;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

//@ApplicationScoped
public class MovieDeserializer extends ObjectMapperDeserializer<Movie> {
    public MovieDeserializer() {
        super(Movie.class);
    }

}
