package com.example.proxy;

import com.example.dto.OmdbData;
import com.example.dto.OmdbResponse;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;

@RegisterRestClient(configKey = "movie-api")
public interface MovieProxy {

    @GET
    OmdbData getByOmdbIdAndTitleAndYear(
            @QueryParam(("i")) String omdbId,
            @QueryParam("t") String title,
            @QueryParam("y") String year
    );

    @GET
    OmdbResponse getBySearch(@QueryParam("s") String search);

}
