package com.example.controller;

import com.example.dto.BasicResponse;
import com.example.model.Movie;
import com.example.dto.OmdbData;
import com.example.dto.OmdbResponse;
import com.example.proxy.MovieProxy;
import com.example.service.MovieProducer;
import com.example.service.MovieService;
import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/movie")
public class MovieController {

    final static Logger LOGGER = LoggerFactory.getLogger(MovieController.class);

    @RestClient
    MovieProxy movieProxy;

    @Inject
    MovieService movieService;

    @Inject
    MovieProducer movieProducer;

    @GET
    @Path("/list")
    public Response getAll() {
        LOGGER.info("Get all movies");
        return Response.ok()
                .entity(movieService.getAll())
                .build();
    }

    @GET
    @Path("/search")
    public Response getBySearchQuery(@QueryParam("q") String query) {
        LOGGER.info("Search movie: {}", query);
        OmdbResponse response = getBySearch(query);
        return Response.ok()
                .entity(response)
                .build();
    }

    @Fallback(fallbackMethod = "fallbackGetBySearchQuery")
    public OmdbResponse getBySearch(String query) {
        return movieProxy.getBySearch(query);
    }

    public OmdbResponse fallbackGetBySearchQuery(String query) {
        return new OmdbResponse();
    }


    @GET
    public Response getDetailsByOmdbIdAndTitleAndYear(
            @QueryParam("omdbId") String omdbId,
            @QueryParam("title") String title,
            @QueryParam("year") String year) {
        OmdbData data = getByOmdbIdAndTitleAndYear(omdbId, title, year);

        Movie movie = new Movie();
        movie.title = data.getTitle();
        movie.year = data.getYear();
        movie.poster = data.getPoster();
        movie.imdbID = data.getImdbID();
        movie.type = data.getType();

        movieProducer.sendMovieToKafka(movie);

        return Response.ok()
                .entity(data)
                .build();
    }

    @Fallback(fallbackMethod = "fallbackGetDetailsByOmdbIdAndTitleAndYear")
    public OmdbData getByOmdbIdAndTitleAndYear(String omdbId, String title, String year) {
        return movieProxy.getByOmdbIdAndTitleAndYear(omdbId, title, year);
    }

    public OmdbData fallbackGetDetailsByOmdbIdAndTitleAndYear(String omdbId, String title, String year) {
        return new OmdbData();
    }

    @POST
    @Path("/save")
    @Transactional
    public Response save(Movie movie) {
//        LOGGER.info("Save movie: {}", data);
        try {
            movieService.save(movie);
            return Response
                    .created(URI.create("/movie/save"))
                    .entity(BasicResponse.created("Movie saved"))
                    .build();
        } catch (BadRequestException e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(BasicResponse.badRequest(e.getMessage()))
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(BasicResponse.error("Internal server error"))
                    .build();
        }
    }


}
